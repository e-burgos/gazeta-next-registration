import { useEffect, useState } from "react";
import "../styles/globals.css";
import Head from "next/head";
import type { AppProps } from "next/app";
import { useLanguage } from "../store/useLanguage";
import { _storage } from "../utils/localStorage";
import { useRouter } from "next/router";
import { PUBLIC_URL } from "../utils/consts";

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { currentLang } = useLanguage();
  const [showChild, setShowChild] = useState(false);

  useEffect(() => {
    setShowChild(true);
    _storage.clearTemporalCache();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (currentLang) {
      router.push(router.asPath, router.asPath, {
        locale: currentLang,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentLang, router.asPath]);

  if (!showChild) return null;

  if (typeof window === "undefined") {
    return <></>;
  } else {
    return (
      <>
        <Head>
          <title>{"Njoftime Falas Nga Celesi"}</title>
          <meta
            name="description"
            content={
              "Doni te mbaroni pune? Njoftime Pune, Shtepi me Qera, Shtepi ne Shitje apo Makina ne shitje? Nese te duhet dicka, celsi i asaj qe po kerkon eshte tek Gazeta Celesi"
            }
          />
          <meta
            name="keywords"
            content={
              "gazeta celsi, gazeta celesi, njoftime falas, marr me qera, me qira, oferta, kerkoj pune, shtepi, apartamente ne Tirane, makina ne shitje, mobilje te perdorura"
            }
          />
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="/apple-icon-57x57.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="/apple-icon-60x60.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="/apple-icon-72x72.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/apple-icon-76x76.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="114x114"
            href="/apple-icon-114x114.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="/apple-icon-120x120.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="144x144"
            href="/apple-icon-144x144.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="/apple-icon-152x152.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-icon-180x180.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="/android-icon-192x192.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href="/favicon-96x96.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <link rel="alternate" href={`${PUBLIC_URL}/al`} hrefLang="al" />
          <link rel="alternate" href={`${PUBLIC_URL}/en`} hrefLang="en" />
          <link rel="alternate" href={`${PUBLIC_URL}/it`} hrefLang="it" />
          <link rel="alternate" href={`${PUBLIC_URL}/es`} hrefLang="es" />
        </Head>
        <main>
          <Component {...pageProps} />
        </main>
      </>
    );
  }
}
