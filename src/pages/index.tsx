import React from "react";
import type { NextPage } from "next";
import AuthLayout from "../components/common/auth-layout";
import Login from "../components/pages/login";

const CategoryPage: NextPage = () => {
  return (
    <AuthLayout>
      <Login />
    </AuthLayout>
  );
};
export default CategoryPage;
