import React from "react";
import type { NextPage } from "next";
import AuthLayout from "../components/common/auth-layout";
import ForgotPassword from "../components/pages/forgot-password";

const ForgotPasswordPage: NextPage = () => {
  return (
    <AuthLayout hideIsotype>
      <ForgotPassword />
    </AuthLayout>
  );
};
export default ForgotPasswordPage;
