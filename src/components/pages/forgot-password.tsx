import React, { useState } from "react";
import styles from "./styles/forgot-password.module.css";
import { colors } from "../ui/palette-colors";
import { useWindowSize } from "../../hooks/useWindowSize";
import { FORGOT_PASSWORD_LABEL } from "../../locates";
import WhiteCard from "../common/white-card";
import useFilterTranslation from "../../hooks/useFilterTranslation";
import Paragraph from "../ui/others/paragraph";
import CommonInput from "../ui/inputs/common-input";
import PrimaryButton from "../ui/buttons/primary-button";
import LinkButton from "../ui/buttons/link-button";
import { useRouter } from "next/navigation";

interface ForgotPasswordProps {}

const ForgotPassword: React.FC<ForgotPasswordProps> = ({}) => {
  // hooks
  const router = useRouter();
  const size = useWindowSize();
  const mobileSize = size && size.width < 768;
  const { handleFilterLang } = useFilterTranslation();

  // states
  const [email, setEmail] = useState<string>("");
  const [validateEmail, setValidateEmail] = useState<boolean>(false);

  return (
    <div className={styles.container}>
      <WhiteCard>
        <div className={styles.headerCard}>
          <Paragraph
            fontFamily="AlbertSans-Bold"
            size={mobileSize ? "24px" : "32px"}
          >
            {handleFilterLang(FORGOT_PASSWORD_LABEL).label}
          </Paragraph>
          <Paragraph
            size={mobileSize ? "14px" : "16px"}
            fontFamily="AlbertSans-Regular"
          >
            {handleFilterLang(FORGOT_PASSWORD_LABEL).text1}
          </Paragraph>
        </div>
        <div className={styles.inputsCard}>
          <CommonInput
            label={handleFilterLang(FORGOT_PASSWORD_LABEL).text2}
            placeholder={handleFilterLang(FORGOT_PASSWORD_LABEL).text3}
            value={email}
            type="email"
            onChange={(e) => setEmail(e.target.value)}
            borderColor="#2424244D"
            size="big"
            color={colors.primary.primary}
            validation
            setValidation={setValidateEmail}
          />
          <PrimaryButton
            style={{ borderRadius: "8px", maxWidth: "360px" }}
            label={handleFilterLang(FORGOT_PASSWORD_LABEL).text4}
            labelSize={mobileSize ? "16px" : "20px"}
            width={"100%"}
            height={"50px"}
            onClick={() => alert("Send email")}
            disabled={!validateEmail}
          />
        </div>
        <div className={styles.footerLink}>
          <LinkButton
            onClick={() => router.push("/")}
            fontFamily="AlbertSans-Regular"
            size={mobileSize ? "12px" : "16px"}
            label={handleFilterLang(FORGOT_PASSWORD_LABEL).text5}
          />
        </div>
      </WhiteCard>
    </div>
  );
};

export default ForgotPassword;
