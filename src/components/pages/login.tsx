import React, { useState } from "react";
import styles from "./styles/login.module.css";
import { useRouter } from "next/router";
import { LOGIN_LABEL } from "../../locates";
import { colors } from "../ui/palette-colors";
import useFilterTranslation from "../../hooks/useFilterTranslation";
import { useWindowSize } from "../../hooks/useWindowSize";
import WhiteCard from "../common/white-card";
import GoogleIcon from "../ui/icons/google-icon";
import FacebookIcon from "../ui/icons/facebook-icon";
import EyeIcon from "../ui/icons/eye-icon";
import EyeSlashIcon from "../ui/icons/eye-slash-icon";
import Paragraph from "../ui/others/paragraph";
import LinkButton from "../ui/buttons/link-button";
import PrimaryButton from "../ui/buttons/primary-button";
import SocialButton from "../ui/buttons/social-button";
import CommonInput from "../ui/inputs/common-input";
import Separator from "../ui/others/separator";

interface LoginProps {}

const Login: React.FC<LoginProps> = ({}) => {
  // hooks
  const router = useRouter();
  const size = useWindowSize();
  const mobileSize = size && size.width < 768;
  const { handleFilterLang } = useFilterTranslation();

  // states
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [password, setPassword] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [validateEmail, setValidateEmail] = useState<boolean>(false);

  // functions
  const handleValidateButton = () => {
    if (!validateEmail || password.length < 8) return true;
    return false;
  };

  return (
    <div className={styles.container}>
      <div className={styles.rightContainer}>
        <WhiteCard>
          <div className={styles.headerCard}>
            <Paragraph
              color={colors.text.light2}
              fontFamily="AlbertSans-Medium"
              size={mobileSize ? "12px" : "16px"}
            >
              {handleFilterLang(LOGIN_LABEL).text1}
            </Paragraph>
            <Paragraph
              fontFamily="AlbertSans-Bold"
              size={mobileSize ? "24px" : "32px"}
            >
              {handleFilterLang(LOGIN_LABEL).label}
            </Paragraph>
            <Paragraph
              size={mobileSize ? "16px" : "24px"}
              fontFamily="AlbertSans-Regular"
            >
              {handleFilterLang(LOGIN_LABEL).text2}
            </Paragraph>
          </div>
          <div className={styles.inputsCard}>
            <CommonInput
              label={handleFilterLang(LOGIN_LABEL).text3}
              placeholder={handleFilterLang(LOGIN_LABEL).text4}
              value={email}
              type="email"
              onChange={(e) => setEmail(e.target.value)}
              borderColor="#2424244D"
              size="big"
              color={colors.primary.primary}
              validation
              setValidation={setValidateEmail}
            />
            <CommonInput
              label={handleFilterLang(LOGIN_LABEL).text5}
              placeholder={handleFilterLang(LOGIN_LABEL).text6}
              helpText={handleFilterLang(LOGIN_LABEL).text7}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              type={showPassword ? "text" : "password"}
              borderColor="#2424244D"
              size="big"
              color={colors.primary.primary}
              badgeBg="transparent"
              badge={
                showPassword ? (
                  <EyeIcon
                    size="24px"
                    onClick={() => setShowPassword(!showPassword)}
                  />
                ) : (
                  <EyeSlashIcon
                    size="24px"
                    onClick={() => setShowPassword(!showPassword)}
                  />
                )
              }
            />
            <LinkButton
              labelColor={colors.categories.blue}
              className={styles.forgotPassword}
              fontFamily="AlbertSans-Regular"
              size={mobileSize ? "14px" : "16px"}
              label={handleFilterLang(LOGIN_LABEL).text8}
              onClick={() => router.push("/forgot-password")}
            />
          </div>
          <PrimaryButton
            style={{ maxWidth: "360px" }}
            label={handleFilterLang(LOGIN_LABEL).text10}
            labelSize={mobileSize ? "16px" : "20px"}
            width={"100%"}
            height={mobileSize ? "50px" : "65px"}
            onClick={() => alert("Login")}
            disabled={handleValidateButton()}
          />
          <div className={styles.separatorContainer}>
            <Separator color="textLight2" width="100%" height="1px" />
            <Paragraph
              size={mobileSize ? "16px" : "20px"}
              fontFamily="AlbertSans-Regular"
            >
              {handleFilterLang(LOGIN_LABEL).text11}
            </Paragraph>
            <Separator color="textLight2" width="100%" height="1px" />
          </div>
          <div className={styles.footerLink}>
            <Paragraph
              size={mobileSize ? "14px" : "18px"}
              fontFamily="AlbertSans-Regular"
            >
              {handleFilterLang(LOGIN_LABEL).text12}
            </Paragraph>
            <LinkButton
              onClick={() => alert("Go to register page")}
              fontFamily="AlbertSans-Bold"
              size={mobileSize ? "14px" : "18px"}
              label={handleFilterLang(LOGIN_LABEL).text13}
            />
          </div>
          <div className={styles.socialButtons}>
            <SocialButton
              label={mobileSize ? "" : handleFilterLang(LOGIN_LABEL).text14}
              customIcon={<GoogleIcon size={mobileSize ? "35px" : "26px"} />}
              labelSize={mobileSize ? "16px" : "18px"}
              width={"100%"}
              height={"50px"}
              onClick={() => alert("Google Account")}
            />
            <SocialButton
              label={mobileSize ? "" : handleFilterLang(LOGIN_LABEL).text15}
              customIcon={<FacebookIcon size={mobileSize ? "35px" : "26px"} />}
              labelSize={mobileSize ? "16px" : "18px"}
              width={"100%"}
              height={"50px"}
              onClick={() => alert("Facebook Account")}
            />
          </div>
        </WhiteCard>
      </div>
    </div>
  );
};

export default Login;
