import React, { FunctionComponent } from "react";

interface Props {
  size?: number;
  color?: string;
  className?: string;
  onClick?: () => void;
}

const LogoIsotype: FunctionComponent<Props> = ({
  size,
  color,
  className,
  onClick,
}) => {
  return (
    <svg
      className={className}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width={size ? 905 * size : "905"}
      height={size ? 1077 * size : "1077"}
      viewBox="0 0 905 1077"
    >
      <defs>
        <clipPath id="clip-path">
          <rect
            id="Rettangolo_3637"
            data-name="Rettangolo 3637"
            width="905"
            height="1077"
            fill={color ? color : "#fff"}
            stroke="#707070"
            strokeWidth="1"
          />
        </clipPath>
        <clipPath id="clip-path-2">
          <path
            id="Tracciato_1132"
            data-name="Tracciato 1132"
            d="M0,1230.424H1186.2V0H0Z"
            fill={color ? color : "#fff"}
          />
        </clipPath>
      </defs>
      <g id="img-login" clipPath="url(#clip-path)">
        <g
          id="Raggruppa_862"
          data-name="Raggruppa 862"
          transform="translate(862.145 1051.612) rotate(180)"
        >
          <g
            id="Gruppo_di_maschere_150"
            data-name="Gruppo di maschere 150"
            clipPath="url(#clip-path-2)"
          >
            <line
              id="Linea_316"
              data-name="Linea 316"
              x1="300.659"
              y1="305.807"
              transform="translate(61.6 0.001)"
              fill="#161615"
            />
            <path
              id="Tracciato_1130"
              data-name="Tracciato 1130"
              d="M0,49.566H0L300.668,355.373,423.852,234.261,244.305,51.639A172.758,172.758,0,0,0,0,49.566"
              transform="translate(0 10.994)"
              fill={color ? color : "#fff"}
            />
            <path
              id="Tracciato_1131"
              data-name="Tracciato 1131"
              d="M899.912,279.634c-79.987-85.7-186.1-130.985-306.861-130.985-122.4,0-234.838,47.4-316.613,133.5-80.332,84.565-124.576,198.395-124.576,320.509,0,129.551,47.465,248.063,133.619,333.725,80.177,79.71,188.263,123.5,304.814,123.5.916,0,1.84,0,2.756-.009,120.058,0,229.906-48.7,310.273-137.144h186.838L1068.3,954.512c-110.332,160.414-283.565,252.416-475.248,252.416-160.492,0-310.186-62.9-421.512-177.094C60.923,916.376,0,764.66,0,602.657,0,441.6,60.474,290.622,170.3,177.535,281.465,63.048,431.6,0,593.051,0c187.9,0,358.619,87.986,468.415,241.4l29.507,41.246H902.728Z"
              transform="translate(95.224 23.504)"
              fill={color ? color : "#fff"}
            />
          </g>
        </g>
      </g>
    </svg>
  );
};
export default LogoIsotype;
