import React, { FunctionComponent } from "react";

interface Props {
  size?: number;
  color?: string;
  className?: string;
  onClick?: () => void;
}

const Logo: FunctionComponent<Props> = ({
  size,
  color,
  className,
  onClick,
}) => {
  return (
    <svg
      className={className}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width={size ? 166.174 * size : "166.174"}
      height={size ? 66.696 * size : "66.696"}
      viewBox="0 0 166.174 66.696"
    >
      <defs>
        <clipPath id="clip-path">
          <rect
            id="Rettangolo_2415"
            data-name="Rettangolo 2415"
            width="166.174"
            height="66.696"
            fill={color ? color : "#fff"}
          />
        </clipPath>
      </defs>
      <g
        id="Raggruppa_146"
        data-name="Raggruppa 146"
        clipPath="url(#clip-path)"
      >
        <path
          id="Tracciato_848"
          data-name="Tracciato 848"
          d="M184.441,78.347H167.69a3.578,3.578,0,0,1-3.579-3.579V54.49a3.578,3.578,0,0,1,3.579-3.579h16.752V56.4h-14.4v5.491h13.913v5.491H170.046v5.472h14.4Z"
          transform="translate(-102.69 -31.857)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_849"
          data-name="Tracciato 849"
          d="M250.306,78.347H232.594a3.054,3.054,0,0,1-3.054-3.053V50.911h5.936V72.856h14.829Z"
          transform="translate(-143.631 -31.857)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_850"
          data-name="Tracciato 850"
          d="M313.588,78.347H296.715a3.458,3.458,0,0,1-3.458-3.458V54.369a3.458,3.458,0,0,1,3.458-3.458h16.873V56.4H299.193v5.491h13.923v5.491H299.193v5.472h14.395Z"
          transform="translate(-183.501 -31.857)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_851"
          data-name="Tracciato 851"
          d="M353.161,59.138a7.394,7.394,0,0,1,.7-3.2,8.4,8.4,0,0,1,1.913-2.612,9.214,9.214,0,0,1,6.277-2.42h13.609V56.4H362.054a3.157,3.157,0,0,0-1.158.21,2.965,2.965,0,0,0-.941.583,2.7,2.7,0,0,0-.631.871,2.54,2.54,0,0,0-.228,1.071,2.6,2.6,0,0,0,.228,1.081,2.672,2.672,0,0,0,.631.881,2.971,2.971,0,0,0,.941.583,3.157,3.157,0,0,0,1.158.21h5.936a9.354,9.354,0,0,1,3.465.641,8.984,8.984,0,0,1,2.834,1.761,8.244,8.244,0,0,1,1.9,2.621,7.737,7.737,0,0,1,0,6.4,8.277,8.277,0,0,1-1.9,2.612,9.123,9.123,0,0,1-2.834,1.77,9.233,9.233,0,0,1-3.465.65H354.815V72.856H367.99a3.143,3.143,0,0,0,1.158-.21,2.969,2.969,0,0,0,.941-.583,2.724,2.724,0,0,0,.631-.871,2.637,2.637,0,0,0,0-2.143,2.72,2.72,0,0,0-.631-.871,2.969,2.969,0,0,0-.941-.583,3.151,3.151,0,0,0-1.158-.21h-5.936a9.217,9.217,0,0,1-6.277-2.42,8.372,8.372,0,0,1-1.913-2.621,7.455,7.455,0,0,1-.7-3.2"
          transform="translate(-220.985 -31.857)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_852"
          data-name="Tracciato 852"
          d="M432.872,78.347h-5.936V50.911h2.634a3.3,3.3,0,0,1,3.3,3.3Z"
          transform="translate(-267.149 -31.857)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_853"
          data-name="Tracciato 853"
          d="M425.613,27.1a3.441,3.441,0,1,1,3.441,3.441,3.441,3.441,0,0,1-3.441-3.441"
          transform="translate(-266.321 -14.807)"
          fill={color ? color : "#fff"}
        />
        <path
          id="Tracciato_854"
          data-name="Tracciato 854"
          d="M56.977,46.565a23.718,23.718,0,1,1,0-27.586,8.833,8.833,0,0,1,7.23-5.127c.062-.018.123-.039.185-.057A32.756,32.756,0,1,0,11.426,52.34L0,63.962a9.53,9.53,0,0,0,13.477-.115l4.694-4.774a32.714,32.714,0,0,0,46.191-7.281c-.14-.039-.277-.083-.414-.128a8.837,8.837,0,0,1-6.971-5.1"
          fill={color ? color : "#fff"}
        />
      </g>
    </svg>
  );
};
export default Logo;
