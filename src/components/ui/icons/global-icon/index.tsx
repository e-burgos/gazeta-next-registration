import React, { FunctionComponent } from "react";

interface Props {
  size?: string;
  color?: string;
  className?: string;
  onClick?: () => void;
}

const GlobalIcon: FunctionComponent<Props> = ({
  size,
  color,
  className,
  onClick,
}) => {
  return (
    <svg
      className={className}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 40.808 40.808"
      width={size ? size : "25px"}
      height={size ? size : "25px"}
    >
      <path
        id="Vector"
        d="M17.529-.75a18.279,18.279,0,0,1,12.925,31.2A18.279,18.279,0,0,1,4.6,4.6,18.159,18.159,0,0,1,17.529-.75Zm0,34.007A15.728,15.728,0,1,0,1.8,17.529,15.746,15.746,0,0,0,17.529,33.257Z"
        transform="translate(2.875 2.875)"
        fill={color ? color : "#000"}
      />
      <path
        id="Vector-2"
        data-name="Vector"
        d="M3.012,32.406h-1.7a1.275,1.275,0,0,1-.037-2.55,49.612,49.612,0,0,1,0-28.056A1.275,1.275,0,0,1,1.312-.75h1.7A1.275,1.275,0,0,1,4.222.929a47.054,47.054,0,0,0,0,29.8,1.275,1.275,0,0,1-1.21,1.679Z"
        transform="translate(12.291 4.576)"
        fill={color ? color : "#000"}
      />
      <path
        id="Vector-3"
        data-name="Vector"
        d="M.526,32.407a1.276,1.276,0,0,1-1.21-1.679,46.956,46.956,0,0,0,2.421-14.9A46.956,46.956,0,0,0-.684.929,1.275,1.275,0,1,1,1.735.121,49.5,49.5,0,0,1,4.287,15.828,49.5,49.5,0,0,1,1.735,31.535,1.276,1.276,0,0,1,.526,32.407Z"
        transform="translate(24.98 4.576)"
        fill={color ? color : "#000"}
      />
      <path
        id="Vector-4"
        data-name="Vector"
        d="M15.828,4.287A49.491,49.491,0,0,1,1.8,2.263,1.275,1.275,0,0,1-.75,2.226V.525A1.275,1.275,0,0,1,.929-.684a46.956,46.956,0,0,0,14.9,2.421,46.956,46.956,0,0,0,14.9-2.421A1.275,1.275,0,0,1,32.406.525v1.7a1.275,1.275,0,0,1-2.55.037A49.491,49.491,0,0,1,15.828,4.287Z"
        transform="translate(4.576 24.98)"
        fill={color ? color : "#000"}
      />
      <path
        id="Vector-5"
        data-name="Vector"
        d="M31.131,4.288a1.273,1.273,0,0,1-.4-.066,47.054,47.054,0,0,0-29.8,0A1.275,1.275,0,1,1,.121,1.8a49.6,49.6,0,0,1,31.414,0,1.275,1.275,0,0,1-.4,2.485Z"
        transform="translate(4.576 12.291)"
        fill={color ? color : "#000"}
      />
      <path
        id="Vector-6"
        data-name="Vector"
        d="M0,0H40.808V40.808H0Z"
        transform="translate(40.808 40.808) rotate(180)"
        fill="none"
        opacity="0"
      />
    </svg>
  );
};
export default GlobalIcon;
