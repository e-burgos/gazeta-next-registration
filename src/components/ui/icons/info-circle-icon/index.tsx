import React, { FunctionComponent } from "react";

interface Props {
  size?: string;
  color?: string;
  className?: string;
  onClick?: () => void;
}

const InfoCircleIcon: FunctionComponent<Props> = ({
  size,
  color,
  className,
  onClick,
}) => {
  return (
    <svg
      className={className}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width={size ? size : "20"}
      height={size ? size : "20"}
      viewBox="0 0 24.279 24.279"
    >
      <g
        id="vuesax_outline_info-circle"
        data-name="vuesax/outline/info-circle"
        transform="translate(-364 -252)"
      >
        <g id="info-circle" transform="translate(364 252)">
          <path
            id="Vector"
            d="M10.875,21.75A10.875,10.875,0,1,1,21.75,10.875,10.884,10.884,0,0,1,10.875,21.75Zm0-20.232a9.357,9.357,0,1,0,9.357,9.357A9.369,9.369,0,0,0,10.875,1.517Z"
            transform="translate(1.265 1.265)"
            fill={color ? color : "#b69100"}
          />
          <path
            id="Vector-2"
            data-name="Vector"
            d="M.759,6.575A.764.764,0,0,1,0,5.817V.759A.764.764,0,0,1,.759,0a.764.764,0,0,1,.759.759V5.817A.764.764,0,0,1,.759,6.575Z"
            transform="translate(11.381 7.334)"
            fill={color ? color : "#b69100"}
          />
          <path
            id="Vector-3"
            data-name="Vector"
            d="M1.012,2.018a1.007,1.007,0,0,1-.384-.081,1.168,1.168,0,0,1-.334-.212,1.044,1.044,0,0,1-.212-.334A1.007,1.007,0,0,1,0,1.007,1.007,1.007,0,0,1,.081.622,1.168,1.168,0,0,1,.293.288,1.168,1.168,0,0,1,.627.076a1.012,1.012,0,0,1,.769,0A1.168,1.168,0,0,1,1.73.288a1.168,1.168,0,0,1,.212.334,1.007,1.007,0,0,1,.081.384,1.007,1.007,0,0,1-.081.384,1.044,1.044,0,0,1-.212.334,1.168,1.168,0,0,1-.334.212A1.007,1.007,0,0,1,1.012,2.018Z"
            transform="translate(11.128 15.179)"
            fill={color ? color : "#b69100"}
          />
          <path
            id="Vector-4"
            data-name="Vector"
            d="M0,0H24.279V24.279H0Z"
            fill="none"
            opacity="0"
          />
        </g>
      </g>
    </svg>
  );
};
export default InfoCircleIcon;
