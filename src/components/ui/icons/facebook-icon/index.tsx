import React, { FunctionComponent } from "react";

interface Props {
  size?: string;
  color?: string;
  className?: string;
  onClick?: () => void;
}

const FacebookIcon: FunctionComponent<Props> = ({
  size,
  color,
  className,
  onClick,
}) => {
  return (
    <svg
      className={className}
      onClick={onClick}
      xmlns="http://www.w3.org/2000/svg"
      width={size ? size : "20"}
      height={size ? size : "20"}
      viewBox="0 0 48 48"
    >
      <path
        fill={color ? color : "#3f51b5"}
        d="M24 4A20 20 0 1 0 24 44A20 20 0 1 0 24 4Z"
      ></path>
      <path
        fill="#fff"
        d="M29.368,24H26v12h-5V24h-3v-4h3v-2.41c0.002-3.508,1.459-5.59,5.592-5.59H30v4h-2.287 C26.104,16,26,16.6,26,17.723V20h4L29.368,24z"
      ></path>
    </svg>
  );
};
export default FacebookIcon;
