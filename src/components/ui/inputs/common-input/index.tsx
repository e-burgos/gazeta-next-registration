import React, { ReactNode, useCallback, useEffect, useState } from "react";
import styles from "./styles/common-input.module.css";
import validator from "validator";
import { IFontFamily } from "../../types";
import { colors } from "../../palette-colors";
import CheckIcon from "../../icons/check-icon";
import Paragraph from "../../others/paragraph";
import InfoCircleIcon from "../../icons/info-circle-icon";

interface CommonInputProps {
  color?: string;
  fontSize?: string;
  fontFamily?: IFontFamily;
  label?: string;
  size?: "large" | "medium" | "small" | "smaller" | "big";
  height?: string;
  width?: string;
  value?: string;
  type?: React.HTMLInputTypeAttribute;
  placeholder?: string;
  borderColor?: string;
  badge?: ReactNode;
  badgeBg?: string;
  helpText?: string;
  validation?: boolean;
  validationSuccess?: boolean;
  validationError?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  autoComplete?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  onKeyDown?: React.KeyboardEventHandler<HTMLInputElement> | undefined;
  setValidation?: (validation: boolean) => void;
}

const CommonInput: React.FC<CommonInputProps> = ({
  color,
  size,
  height,
  width,
  value,
  type,
  label,
  borderColor,
  placeholder,
  badge,
  badgeBg,
  helpText,
  fontSize,
  fontFamily,
  validation,
  validationSuccess,
  validationError,
  disabled,
  readOnly,
  autoComplete,
  setValidation,
  onChange,
  onKeyDown,
}) => {
  const [focus, setFocus] = useState<boolean>(false);
  const [hover, setHover] = useState<boolean>(false);
  const [validate, setValidate] = useState<boolean>(false);

  const handleValidation = useCallback(() => {
    if (type === "email" && value) {
      validator.isEmail(value) ? setValidate(true) : setValidate(false);
    }
    if (type === "tel" && value) {
      validator.isMobilePhone(value) ? setValidate(true) : setValidate(false);
    }
    if (type === "phone" && value) {
      validator.isMobilePhone(value, ["es-AR", "sq-AL", "it-IT"])
        ? setValidate(true)
        : setValidate(false);
    }
    if (type === "number" && value) {
      validator.isNumeric(value) ? setValidate(true) : setValidate(false);
    }
  }, [type, value]);

  useEffect(() => {
    handleValidation();
    setValidation && setValidation(validate);
  }, [handleValidation, setValidation, validate]);

  return (
    <div
      className={styles.container}
      style={{
        width: "100%",
        maxWidth:
          size === "big"
            ? "100%"
            : size === "large"
            ? "480px"
            : size === "medium"
            ? "350px"
            : size === "small"
            ? "225px"
            : size === "smaller"
            ? "112px"
            : width,
        height: height,
      }}
    >
      {label && !helpText && (
        <Paragraph className={styles.label}>{label}</Paragraph>
      )}
      {label && helpText && (
        <div className={styles.helpText}>
          <Paragraph className={styles.labelHelpText}>{label}</Paragraph>
          <div className={styles.helpTextContent}>
            <InfoCircleIcon color="#000" size="18px" />
            <div className={styles.tooltip}>
              <Paragraph size="14px">{helpText}</Paragraph>
            </div>
          </div>
        </div>
      )}
      <input
        className={styles.input}
        autoComplete={autoComplete || "off"}
        type={type ? type : "text"}
        placeholder={placeholder}
        value={value}
        readOnly={readOnly}
        disabled={disabled}
        onChange={onChange}
        onKeyDown={onKeyDown}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        style={{
          fontSize: fontSize,
          fontFamily: fontFamily ? fontFamily : "AlbertSans-Regular",
          maxWidth:
            size === "big"
              ? "100%"
              : size === "large"
              ? "480px"
              : size === "medium"
              ? "350px"
              : size === "small"
              ? "225px"
              : size === "smaller"
              ? "112px"
              : width,
          height: height,
          border:
            hover && color
              ? `1px solid ${color}50`
              : hover && !color
              ? `1px solid ${colors.primary.primary}`
              : focus && color
              ? `1px solid ${color}`
              : focus && !color
              ? `1px solid ${colors.primary.primary}`
              : borderColor
              ? `1px solid ${borderColor}`
              : "",
          boxShadow:
            hover && color
              ? `0px 3px 6px ${color}30`
              : hover && !color
              ? `0px 3px 6px ${colors.primary.primary}30`
              : "",
          paddingRight: badge || validation ? "44px" : "",
        }}
      />
      {badge && (
        <div style={{ background: badgeBg }} className={styles.badge}>
          {badge}
        </div>
      )}
      {type && validation && (
        <div style={{ background: badgeBg }} className={styles.badge}>
          {validate ? (
            <CheckIcon color={colors.categories.green} size="24px" />
          ) : (
            <InfoCircleIcon color={colors.categories.red} size="24px" />
          )}
        </div>
      )}
      {validationSuccess && (
        <div style={{ background: badgeBg }} className={styles.badge}>
          <CheckIcon color={colors.categories.green} size="24px" />
        </div>
      )}
      {validationError && (
        <div style={{ background: badgeBg }} className={styles.badge}>
          <InfoCircleIcon color={colors.categories.red} size="24px" />
        </div>
      )}
    </div>
  );
};

export default CommonInput;
