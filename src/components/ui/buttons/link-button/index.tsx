import React, { FunctionComponent, ReactNode } from "react";
import styles from "./styles/link-button.module.css";
import { IFontFamily } from "../../types";
import Paragraph from "../../others/paragraph";

interface LinkButtonProps {
  onClick?: () => void;
  label: string;
  size?: string;
  disabled?: boolean;
  className?: string;
  labelColor?: string;
  active?: boolean;
  icon?: ReactNode;
  rightIcon?: ReactNode;
  fontFamily?: IFontFamily;
}

const LinkButton: FunctionComponent<LinkButtonProps> = ({
  onClick: handleClick,
  label,
  disabled,
  className,
  labelColor,
  active,
  size,
  icon,
  rightIcon,
  fontFamily,
}) => {
  return (
    <a
      className={`${styles.button} ${disabled && styles.disabled} ${className}`}
      onClick={handleClick}
    >
      <div className={styles.content}>
        {icon && icon}
        <Paragraph
          className={`${!disabled && styles.label} ${active && styles.active}`}
          fontFamily={fontFamily ? fontFamily : "AlbertSans-Medium"}
          size={size ? size : "16px"}
          color={
            disabled
              ? "#1A1A1A30"
              : labelColor && !disabled
              ? labelColor
              : "#1A1A1A"
          }
        >
          {label}
        </Paragraph>
        {rightIcon && rightIcon}
      </div>
    </a>
  );
};
export default LinkButton;
