import React, { FunctionComponent, ReactNode } from "react";
import styles from "./styles/primary-button.module.css";
import { IFontFamily } from "../../types";
import Spinner from "../../icons/spinner/Spinner";
import Paragraph from "../../others/paragraph";

interface PrimaryButtonProps {
  onClick?: () => void;
  label?: string;
  width?: string;
  height?: string;
  labelColor?: string;
  disabled?: boolean;
  active?: boolean;
  loading?: boolean;
  customIcon?: ReactNode;
  fontFamily?: IFontFamily;
  labelSize?: string;
  style?: React.CSSProperties;
}

const PrimaryButton: FunctionComponent<PrimaryButtonProps> = ({
  onClick: handleClick,
  label,
  labelColor,
  width,
  height,
  disabled,
  loading,
  active,
  customIcon,
  fontFamily,
  labelSize,
  style,
}) => {
  return (
    <button
      disabled={disabled}
      className={`${styles.button} 
      ${disabled && styles.disabled} 
      ${loading && styles.buttonLoading}`}
      onClick={handleClick}
      style={{
        width: width,
        height: height,
        backgroundColor: active ? "#FACF16" : "",
        ...style,
      }}
    >
      <div className={styles.labelContainer}>
        {loading && (
          <Spinner
            color={disabled ? "#FACF16" : active ? "#FFFFFF" : "#000000"}
            size="small"
          />
        )}

        {customIcon && customIcon}
        {label && (
          <Paragraph
            className={`${active && styles.active}`}
            fontFamily={fontFamily ? fontFamily : "AlbertSans-Regular"}
            size={labelSize ? labelSize : "20px"}
            color={
              disabled
                ? "#00000009"
                : active
                ? "#FFFFFF"
                : labelColor && !disabled
                ? labelColor
                : "#000000"
            }
          >
            {label}
          </Paragraph>
        )}
      </div>
    </button>
  );
};

export default PrimaryButton;
