export type IFontFamily =
  | "AlbertSans-Regular"
  | "AlbertSans-Medium"
  | "AlbertSans-Bold"
  | "AlbertSans-Light";
