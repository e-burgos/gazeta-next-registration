import React, { FunctionComponent } from "react";
import styles from "./styles/separator.module.css";
import { IColors } from "../../palette-colors";
import { handleColor } from "../../../../utils/functions";

interface SeparatorProps {
  color?: IColors;
  customColor?: string;
  width?: string;
  height?: string;
  className?: string;
  style?: React.CSSProperties;
}

const Separator: FunctionComponent<SeparatorProps> = ({
  color,
  customColor,
  width,
  height,
  className,
  style,
}) => {
  return (
    <div
      style={{
        background: `${customColor ? customColor : handleColor(color)}`,
        width: width,
        minHeight: height,
        maxHeight: height,
        ...style,
      }}
      className={`${styles.container} ${className}`}
    ></div>
  );
};

export default Separator;
