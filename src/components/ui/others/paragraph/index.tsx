import React, { FunctionComponent } from "react";
import { IFontFamily } from "../../types";

interface ParagraphProps {
  "aria-label"?: string;
  htmlTag?: "H1" | "H2" | "H3" | "H4" | "H5" | "H6";
  children: string | String | undefined;
  color?: string;
  fontFamily?: IFontFamily;
  size?: string;
  className?: string;
  style?: React.CSSProperties | undefined;
  onClick?: () => void;
}

const Paragraph: FunctionComponent<ParagraphProps> = ({
  "aria-label": ariaLabel,
  htmlTag,
  children,
  color,
  size,
  fontFamily,
  className,
  style,
  onClick,
}) => {
  const handleHtmlTag = () => {
    switch (htmlTag) {
      case "H1":
        return (
          <h1
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </h1>
        );
      case "H2":
        return (
          <h2
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </h2>
        );
      case "H3":
        return (
          <h3
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children || ""}
          </h3>
        );
      case "H4":
        return (
          <h4
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </h4>
        );
      case "H5":
        return (
          <h5
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </h5>
        );
      case "H6":
        return (
          <h6
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </h6>
        );
      default:
        return (
          <p
            onClick={onClick}
            aria-label={ariaLabel}
            className={`${className}`}
            style={{
              color: `${color ? color : "#000000"}`,
              fontSize: `${size ? size : "14px"}`,
              fontFamily: `${fontFamily ? fontFamily : "AlbertSans-Regular"}`,
              ...style,
            }}
          >
            {children}
          </p>
        );
    }
  };

  return <>{handleHtmlTag()}</>;
};

export default Paragraph;
