export type IColors =
  | "textNormal"
  | "textLight"
  | "textLight2"
  | "title"
  | "background"
  | "backgroundElement"
  | "borderLight"
  | "black"
  | "white"
  | "lightGrey"
  | "darkGrey"
  | "focus"
  | "success"
  | "warning"
  | "alert"
  | "blue"
  | "red"
  | "purple"
  | "green"
  | "orange"
  | "primary"
  | "primaryDisabled"
  | "primaryDisabled2"
  | "secondary"
  | "secondaryDisabled"
  | "transparent"
  | "";

export const colors = {
  background: "#F8F8F8",
  backgroundElement: "#24242414",
  border: {
    light: "#29292930",
  },
  text: {
    normal: "#1A1A1A",
    light: "#1A1A1A30",
    light2: "#00000065",
    light3: "#2C2C2CAC",
    dark: "#000000",
    title: "#242424",
  },
  primary: {
    primary: "#facf16",
    primaryDisabled: "#facf1650",
    primaryDisabled2: "#B69100",
  },
  secondary: {
    secondary: "#242424",
    secondaryDisabled: "#00000030",
  },
  categories: {
    blue: "#5C99F8",
    red: "#F27A5D",
    purple: "#7F63F5",
    green: "#88D373",
    orange: "#F5C263",
  },
  states: {
    focus: "#FACF165A",
    success: "#5ECB7F",
    warning: "#D8CB6C",
    alert: "#FF5234",
  },
  neutrals: {
    black: "#000000",
    white: "#ffffff",
    lightGrey: "#D5D5D5",
    darkGrey: "#1A1A1A",
  },
};
