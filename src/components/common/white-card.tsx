import React, { ReactNode } from "react";
import styles from "./styles/white-card.module.css";

interface WhiteCardProps {
  children: ReactNode;
  register?: boolean;
  style?: React.CSSProperties;
}

const WhiteCard: React.FC<WhiteCardProps> = ({ children, register }) => {
  return (
    <div
      style={{ justifyContent: register ? "space-between" : "" }}
      className={`${styles.whiteCard} ${register && styles.whiteCardRegister}`}
    >
      {children}
    </div>
  );
};

export default WhiteCard;
