import React, { useState } from "react";
import styles from "./styles/language-button.module.css";
import { useLanguage } from "../../store/useLanguage";
import { ILocates } from "../../utils/types";
import GlobalIcon from "../ui/icons/global-icon";
import Paragraph from "../ui/others/paragraph";
import { colors } from "../ui/palette-colors";

interface LanguageButtonProps {}

const LanguageButton: React.FC<LanguageButtonProps> = ({}) => {
  const { currentLang, setCurrentLang } = useLanguage();
  const [openLaguage, setOpenLanguage] = useState(false);

  const handleLang = (value: ILocates) => {
    if (value === "al") return "AL";
    if (value === "it") return "ITA";
    if (value === "es") return "ES";
    if (value === "en") return "EN";
  };

  return (
    <button
      onClick={() => setOpenLanguage(!openLaguage)}
      className={styles.languageButton}
    >
      <GlobalIcon size={"20"} />
      <Paragraph size="16px" fontFamily="AlbertSans-Bold">
        {`${handleLang(currentLang)}`}
      </Paragraph>
      {openLaguage && (
        <div className={styles.languageContainer}>
          <div
            className={styles.languageItem}
            onClick={() => setCurrentLang("al")}
          >
            <div className={styles.languageLi} />
            <Paragraph
              size="12px"
              fontFamily="AlbertSans-Bold"
              color={
                currentLang !== "al" ? colors.text.light : colors.text.dark
              }
            >
              {"AL"}
            </Paragraph>
          </div>
          <div
            className={styles.languageItem}
            onClick={() => setCurrentLang("it")}
          >
            <div className={styles.languageLi} />
            <Paragraph
              size="12px"
              fontFamily="AlbertSans-Bold"
              color={
                currentLang !== "it" ? colors.text.light : colors.text.dark
              }
            >
              {"ITA"}
            </Paragraph>
          </div>
          <div
            className={styles.languageItem}
            onClick={() => setCurrentLang("en")}
          >
            <div className={styles.languageLi} />
            <Paragraph
              size="12px"
              fontFamily="AlbertSans-Bold"
              color={
                currentLang !== "en" ? colors.text.light : colors.text.dark
              }
            >
              {"EN"}
            </Paragraph>
          </div>
          <div
            className={styles.languageItem}
            onClick={() => setCurrentLang("es")}
          >
            <div className={styles.languageLi} />
            <Paragraph
              size="12px"
              fontFamily="AlbertSans-Bold"
              color={
                currentLang !== "es" ? colors.text.light : colors.text.dark
              }
            >
              {"ES"}
            </Paragraph>
          </div>
        </div>
      )}
    </button>
  );
};

export default LanguageButton;
