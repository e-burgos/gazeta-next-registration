import React, { ReactNode } from "react";
import styles from "./styles/auth-layout.module.css";
import LogoIsotype from "../ui/icons/logo-isotype";
import Logo from "../ui/icons/logo";
import Paragraph from "../ui/others/paragraph";
import { LOGO_LABEL } from "../../locates";
import { useWindowSize } from "../../hooks/useWindowSize";
import useFilterTranslation from "../../hooks/useFilterTranslation";
import LanguageButton from "./language-button";

interface AuthLayoutProps {
  children: ReactNode;
  hideIsotype?: boolean;
}

const AuthLayout: React.FC<AuthLayoutProps> = ({ children, hideIsotype }) => {
  // hooks
  const size = useWindowSize();
  const mobileWidth = size && size.width < 768;
  const { handleFilterLang } = useFilterTranslation();

  return (
    <div className={styles.wrapper}>
      {!mobileWidth && (
        <>
          {!hideIsotype && (
            <LogoIsotype size={0.8} className={styles.isotype} />
          )}
        </>
      )}
      <header className={styles.header}>
        <div className={styles.logo}>
          <Logo
            color="#000"
            size={mobileWidth ? 0.8 : 1}
            onClick={() => alert("Go to home page")}
          />
          {!mobileWidth && (
            <Paragraph
              className={styles.logoText}
              size="14px"
              fontFamily="AlbertSans-Medium"
            >
              {handleFilterLang(LOGO_LABEL).label}
            </Paragraph>
          )}
        </div>
        <LanguageButton />
      </header>
      <main className={styles.content}>{children}</main>
    </div>
  );
};

export default AuthLayout;
