import { IFilterLangSelector } from "../utils/types";

export const LOGO_LABEL: IFilterLangSelector = {
  label: {
    al: "Doni të mbaroni punë?",
    en: "Do you want to finish the job?",
    es: "¿Quieres terminar el trabajo?",
    it: "Vuoi finire il lavoro?",
  },
};

export const LOGIN_LABEL: IFilterLangSelector = {
  label: {
    it: "Effettua il login",
    en: "Login",
    es: "Inicia sesión",
    al: "Hyr",
  },
  text1: {
    it: "HEY! COSA ASPETTI",
    en: "HEY! WHAT ARE YOU WAITING FOR",
    es: "¡OYE! ¿QUÉ ESTÁS ESPERANDO?",
    al: "HEY! ÇKA PO PRESIM",
  },
  text2: {
    it: "Compra e vendi in una community unita e sicura",
    en: "Buy and sell in a united and secure community",
    es: "Compra y vende en una comunidad unida y segura",
    al: "Blej dhe shite në një komunitet të bashkuar dhe të sigurt",
  },
  text3: {
    it: "La tua email",
    en: "Your email",
    es: "Tu correo electrónico",
    al: "Email-i juaj",
  },
  text4: {
    es: "Ingresa un correo electrónico válido",
    en: "Enter a valid email",
    it: "Inserisci una email valida",
    al: "Shkruani një email të vlefshme",
  },
  text5: {
    it: "La tua password",
    en: "Your password",
    es: "Tu contraseña",
    al: "Fjalëkalimi juaj",
  },
  text6: {
    it: "Inserisci la tua password",
    en: "Enter your password",
    es: "Ingresa tu contraseña",
    al: "Shkruani fjalëkalimin tuaj",
  },
  text7: {
    es: "Tu contraseña debe tener al menos 8 caracteres y complir con los requisitos de seguridad.",
    en: "Your password must be at least 8 characters long and comply with security requirements.",
    it: "La tua password deve essere lunga almeno 8 caratteri e rispettare i requisiti di sicurezza.",
    al: "Fjalëkalimi juaj duhet të jetë të paktën 8 karaktere i gjatë dhe të përmbushë kërkesat e sigurisë.",
  },
  text8: {
    it: "Non ricordo la password",
    en: "I don't remember the password",
    es: "No recuerdo la contraseña",
    al: "Nuk e mbaj mend fjalëkalimin",
  },
  text10: {
    it: "Accedi",
    en: "Login",
    es: "Iniciar sesión",
    al: "Hyr",
  },
  text11: {
    it: "Oppure",
    en: "Or",
    es: "O",
    al: "Ose",
  },
  text12: {
    it: "Non hai un account Celesi?",
    en: "Don't have a Celesi account?",
    es: "¿No tienes una cuenta Celesi?",
    al: "Nuk keni një llogari Celesi?",
  },
  text13: {
    it: "Registrati ora",
    en: "Register now",
    es: "Regístrate ahora",
    al: "Regjistrohuni tani",
  },
  text14: {
    it: "Accedi con Google",
    en: "Login with Google",
    es: "Inicia sesión con Google",
    al: "Hyr me Google",
  },
  text15: {
    it: "Accedi con Facebook",
    en: "Login with Facebook",
    es: "Inicia sesión con Facebook",
    al: "Hyr me Facebook",
  },
};

export const FORGOT_PASSWORD_LABEL: IFilterLangSelector = {
  label: {
    it: "Hai dimenticato la password?",
    en: "Forgot your password?",
    es: "¿Olvidaste tu contraseña?",
    al: "Keni harruar fjalëkalimin?",
  },
  text1: {
    it: "Nessun problema! Manderemo una mail per resettare la tua password all’indirizzo che inserirai qui sotto.",
    en: "No problem! We will send an email to reset your password to the address you will enter below.",
    es: "¡No hay problema! Enviaremos un correo electrónico para restablecer su contraseña a la dirección que ingresará a continuación.",
    al: "Pa problem! Do t'ju dërgojmë një email për të rivendosur fjalëkalimin tuaj në adresën që do të vendosni më poshtë.",
  },
  text2: {
    it: "La tua email",
    en: "Your email",
    es: "Tu correo electrónico",
    al: "Email-i juaj",
  },
  text3: {
    it: "Inserisci la tua email",
    en: "Enter your email",
    es: "Ingresa tu correo electrónico",
    al: "Shkruani email-in tuaj",
  },
  text4: {
    it: "Manda email",
    en: "Send email",
    es: "Enviar correo electrónico",
    al: "Dërgo email",
  },
  text5: {
    it: "Torna al login",
    en: "Back to login",
    es: "Volver al inicio de sesión",
    al: "Kthehu te hyrja",
  },
};
