import { create } from "zustand";
import { ILocates } from "../utils/types";
import { _storage } from "../utils/localStorage";

export interface ILanguage {
  currentLang: ILocates;
  setCurrentLang: (value: ILocates) => void;
}

export const useLanguage = create<ILanguage>((set) => {
  const lang = _storage.get("current-lang");
  return {
    currentLang: lang ? lang : "al",
    setCurrentLang: (value) => {
      _storage.set("current-lang", value);
      set({ currentLang: value });
    },
  };
});
