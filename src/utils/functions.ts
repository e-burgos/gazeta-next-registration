import { IColors, colors } from "../components/ui/palette-colors";

export const handleColor = (color?: IColors) => {
  return color === "blue"
    ? colors.categories.blue
    : color === "red"
    ? colors.categories.red
    : color === "purple"
    ? colors.categories.purple
    : color === "green"
    ? colors.categories.green
    : color === "orange"
    ? colors.categories.orange
    : color === "alert"
    ? colors.states.alert
    : color === "focus"
    ? colors.states.focus
    : color === "success"
    ? colors.states.success
    : color === "warning"
    ? colors.states.warning
    : color === "black"
    ? colors.neutrals.black
    : color === "darkGrey"
    ? colors.neutrals.darkGrey
    : color === "lightGrey"
    ? colors.neutrals.lightGrey
    : color === "white"
    ? colors.neutrals.white
    : color === "primary"
    ? colors.primary.primary
    : color === "primaryDisabled"
    ? colors.primary.primaryDisabled
    : color === "primaryDisabled2"
    ? colors.primary.primaryDisabled2
    : color === "secondary"
    ? colors.secondary.secondary
    : color === "secondaryDisabled"
    ? colors.secondary.secondaryDisabled
    : color === "textLight"
    ? colors.text.light
    : color === "textNormal"
    ? colors.text.normal
    : color === "textLight2"
    ? colors.text.light2
    : color === "title"
    ? colors.text.title
    : color === "background"
    ? colors.background
    : color === "backgroundElement"
    ? colors.backgroundElement
    : color === "borderLight"
    ? colors.background
    : color === "transparent"
    ? "transparent"
    : colors.primary.primary;
};
