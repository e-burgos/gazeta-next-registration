// ENVs
export const PUBLIC_URL = process.env.NEXT_PUBLIC_URL || "";

// Cache time
export const MINUTE_IN_MILISECONDS = 60000;
export const THREE_MINUTES_IN_MILISECONDS = MINUTE_IN_MILISECONDS * 3;
export const FIVE_MINUTES_IN_MILISECONDS = MINUTE_IN_MILISECONDS * 5;
export const TEN_MINUTES_IN_MILISECONDS = MINUTE_IN_MILISECONDS * 10;
export const THIRTY_MINUTES_IN_MILISECONDS = MINUTE_IN_MILISECONDS * 30;
export const HOUR_IN_MILISECONDS = MINUTE_IN_MILISECONDS * 60;
export const DAY_IN_MILISECONDS = HOUR_IN_MILISECONDS * 24;

// Max attachment file size allowed: 5MB
export const MAX_ATTACHMENT_FILE_SIZE = 1024 * 1024 * 5;
export const ONE_MB = 1024 * 1024 * 1;

// Scopes
export const LOGIN_TOKEN_SCOPES =
  "profile:full_access gazeta-ads:full_access profile:show profile:update profile:delete";

export const ALL_TOKEN_SCOPES =
  "profile:full_access gazeta-ads:full_access profile:show profile:update profile:delete customers:store customers:index loyalty-cards:update loyalty-cards:show verifications:store contact-verifications:store contact-verifications:update contact-verifications:delete contact-verifications:show contacts:verify";
