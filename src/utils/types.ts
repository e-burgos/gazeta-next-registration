export type ILocates = "al" | "it" | "en" | "es";

export interface IFilterOption {
  previousLabel?: string;
  label: string;
  value: string | number;
}

export interface IFilterLangSelector {
  label: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  placeholder?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  options?: {
    it: IFilterOption[];
    al: IFilterOption[];
    en: IFilterOption[];
    es: IFilterOption[];
  };
  text1?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text2?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text3?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text4?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text5?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text6?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text7?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text8?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text9?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text10?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text11?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text12?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text13?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text14?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text15?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
}

export interface IFilterLangSelectorResult {
  label: string;
  options: IFilterOption[];
  placeholder: string;
  text1: string;
  text2: string;
  text3: string;
  text4: string;
  text5: string;
  text6: string;
  text7: string;
  text8: string;
  text9: string;
  text10: string;
  text11: string;
  text12: string;
  text13: string;
  text14: string;
  text15: string;
}
export interface IFilterSelector {
  label: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  placeholder?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  options?: IFilterOption[];
  text1?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text2?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text3?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text4?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text5?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text6?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text7?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text8?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text9?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text10?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text11?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text12?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text13?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text14?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
  text15?: {
    it: string;
    al: string;
    en: string;
    es: string;
  };
}

export interface IFilterSelectorResult {
  label: string;
  options: IFilterOption[];
  placeholder: string;
  text1: string;
  text2: string;
  text3: string;
  text4: string;
  text5: string;
  text6: string;
  text7: string;
  text8: string;
  text9: string;
  text10: string;
  text11: string;
  text12: string;
  text13: string;
  text14: string;
  text15: string;
}
