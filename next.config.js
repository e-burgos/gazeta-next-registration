/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n: {
    locales: ["es", "en", "al", "it"],
    defaultLocale: "al",
    localeDetection: false,
  },
  images: {
    unoptimized: true,
  },
};

module.exports = nextConfig;
